import re
import nltk
from afinn import Afinn
from nltk.corpus import stopwords


class AfinnOwn:
    def __init__(self):
        # Deklaration
        self.afinn = Afinn()

        # Init - nltk donwloads
        nltk.download('stopwords')

    def __cleanComment(self, comment):
        comment = re.findall("[A-Za-z]+", comment.lower())

        stop_words = set(stopwords.words('english'))

        # Delete Stopwords
        comment = [w for w in comment if not w in stop_words]

        return comment

    def score(self, comment):
        return self.__convert4decimal(self.afinn.score(comment))

    def score_words(self, comment):
        # Deklaration
        words_score = []

        # Cleaning Comment
        comment = self.__cleanComment(comment)
        comment = list(dict.fromkeys(comment))

        for word in comment:
            words_score.append(self.afinn.score(word))

        return comment, words_score

    def __convert4decimal(self, number):
        return (int(number*10000))/10000
