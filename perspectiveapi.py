import json
import requests
import re
import nltk
from nltk.corpus import stopwords


class PerspectiveAPI:
    def __init__(self, apiKey):
        # Deklaration
        self.apiKey = apiKey
        self.url    = ('https://commentanalyzer.googleapis.com/v1alpha1/comments:analyze?key=' + self.apiKey)

        # Init - nltk donwloads
        nltk.download('stopwords')

    def __cleanComment(self, comment):
        comment = re.findall("[A-Za-z]+", comment.lower())

        stop_words = set(stopwords.words('english'))

        # Delete Stopwords
        comment = [w for w in comment if not w in stop_words]

        return comment

    def score(self, comment):
        data_dict = {
            'comment': {'text': comment},
            'languages': ['en'],
            'requestedAttributes': {'TOXICITY': {}}
        }

        while True:
            response    = requests.post(url=self.url, data=json.dumps(data_dict))
            objectscore = json.loads(response.content)
            if 'attributeScores' in objectscore:
                objectscore = objectscore['attributeScores']['TOXICITY']['summaryScore']['value']
                break

        return self.__convert4decimal(objectscore)

    def score_words(self, comment):
        # Deklaration
        words_score = []

        # Cleaning Comment
        comment = self.__cleanComment(comment)
        comment = list(dict.fromkeys(comment))

        for word in comment:
            data_dict = {
                'comment': {'text': word},
                'languages': ['en'],
                'requestedAttributes': {'TOXICITY': {}}
            }
            while True:
                response = requests.post(url=self.url, data=json.dumps(data_dict))
                objectscore = json.loads(response.content)
                if 'attributeScores' in objectscore:
                    objectscore = objectscore['attributeScores']['TOXICITY']['summaryScore']['value']
                    words_score.append((objectscore - 0.5) * -1)
                    break

        return comment, words_score

    def __convert4decimal(self, number):
        return (int(number*10000))/10000
