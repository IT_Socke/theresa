from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import *
from multiprocessing import Pool, cpu_count
from sentiWordNet import SentiWordNet
from perspectiveapi import PerspectiveAPI
from reddit_helper import RedditAnalyse
from config import Config
from functools import partial
from own_afinn import AfinnOwn

import numpy as np
import time
import sys


class GUI(QMainWindow):
    def __init__(self, parent=None):
        super(GUI, self).__init__(parent)

        # Deklaration
        self.comments       = []
        self.bReturn        = "None"
        self.senti          = SentiWordNet("SentiWordNet_3.0.0.txt")
        self.config         = Config()
        self.reddit_para    = self.config.getRedditValues()
        self.perspec_para   = self.config.getPerspectiveValues()
        self.afinn          = AfinnOwn()
        self.perspective    = PerspectiveAPI(self.perspec_para[0][1])

        # load GUI
        uic.loadUi('mainwindow.ui', self)

        # Init GUI
        self.__initGUI()

        # Show the GUI
        self.show()

    def __initGUI(self):
        # Button
        self.button = self.findChild(QtWidgets.QPushButton, "btn_go")
        self.button.clicked.connect(self.goButtonPressed)

        # Input
        self.input = self.findChild(QtWidgets.QLineEdit, "le_URL")

        # TreeWidget
        self.treeWidget = self.findChild(QtWidgets.QTreeView, "treeW_comments")
        self.treeWidget.setColumnWidth(0, 300)
        self.treeWidget.hideColumn(1)
        self.treeWidget.currentItemChanged.connect(self.displayComment)

        # plainTextEdit
        self.comment_plaintextview = self.findChild(QtWidgets.QPlainTextEdit, "pte_comment")
        self.pos_words = self.findChild(QtWidgets.QPlainTextEdit, "pte_posScoreWords")
        self.neg_words = self.findChild(QtWidgets.QPlainTextEdit, "pte_negScoreWords")

        # Label
        self.lbl_min = self.findChild(QtWidgets.QLabel, "lbl_minScoreSet")
        self.lbl_max = self.findChild(QtWidgets.QLabel, "lbl_maxScoreSet")
        self.lbl_avg = self.findChild(QtWidgets.QLabel, "lbl_avgScoreSet")

        # RadioButton
        self.rbtn_afinn             = self.findChild(QtWidgets.QRadioButton, "rbtn_afinn")
        self.rbtn_senti             = self.findChild(QtWidgets.QRadioButton, "rbtn_senti")
        self.rbtn_perspectiveapi    = self.findChild(QtWidgets.QRadioButton, "rbtn_perspective")

        # Menu
        self.menubar        = self.findChild(QtWidgets.QMenu, "menu_file")
        self.mb_settings    = self.findChild(QtWidgets.QAction, "act_settings")
        self.mb_settings.triggered.connect(self.showSettings)

    def buildtree(self, treeView, list, level, idx, parent):
        # Deklaration
        number = 1

        while idx < len(list):
            # where should the item append
            if parent is None:
                item = QTreeWidgetItem(treeView)
            else:
                item = QTreeWidgetItem(parent)

            # check the level
            if list[idx][1] < level:
                return idx - 1

            # Set Values
            item.setText(0, str(number))
            item.setText(1, str(idx))
            item.setText(2, list[idx][0])
            item.setText(3, str(list[idx][3]))
            item.setText(4, str(list[idx][5]))

            # based on id and level ind the recursion
            if idx+1 == len(list):
                return idx
            elif list[idx+1][1] > level:
                idx = self.buildtree(treeView, list, level+1, idx+1, item)
            elif list[idx+1][1] < level:
                return idx

            # increase
            number  += 1
            idx     += 1
        return idx

    def goButtonPressed(self):
        # Deklaration
        url         = self.input.text()
        submission  = RedditAnalyse(url, self.reddit_para[0][1], self.reddit_para[1][1], self.reddit_para[2][1],
                                    self.reddit_para[3][1], self.reddit_para[4][1], 20)

        # Start Time
        now = time.time()

        # Get Comments
        self.comments  = submission.edit_article()
        del(self.comments[0])

        # set scoring function
        if self.rbtn_afinn.isChecked():
            scoring_fnc = self.afinn.score
        elif self.rbtn_senti.isChecked():
            scoring_fnc = self.senti.score
        else:
            scoring_fnc = self.perspective.score

        # take the fastest way by number
        if len(self.comments) < 100:
            for comment in self.comments:
                comment[3]  = scoring_fnc(comment[4])
            scores      = (np.take(self.comments, 3, axis=1)).astype(float)
        else:
            # Multiprocessing
            cores   = cpu_count()
            pool    = Pool(cores)
            scores  = pool.map(scoring_fnc, np.take(self.comments, 4, axis=1))

            holder1         = np.array(self.comments)
            holder1[:, 3]   = np.array(scores)
            self.comments   = holder1

        # Statistics build
        avg     = np.mean(scores)
        maxi    = np.max(scores)
        mini    = np.min(scores)

        # Set Textboxes
        self.lbl_min.setText(str(self.__convert2decimal(mini)))
        self.lbl_max.setText(str(self.__convert2decimal(maxi)))
        self.lbl_avg.setText(str(self.__convert2decimal(avg)))

        # Build Commentarytree
        self.treeWidget.clear()
        self.buildtree(self.treeWidget, self.comments, 1, 0, None)

        # Show Timediff
        later = time.time()
        difference = int((later - now)*1000) / 1000
        print("Total Time:{}".format(difference))

    def showSettings(self):
        # Deklaration
        dialog      = QDialog(self)
        dialog.ui   = uic.loadUi('settings.ui', dialog)

        # Set LineEdit texts
        dialog.findChild(QLineEdit, "le_client_id").setText(self.reddit_para[0][1])
        dialog.findChild(QLineEdit, "le_client_secret").setText(self.reddit_para[1][1])
        dialog.findChild(QLineEdit, "le_user_agent").setText(self.reddit_para[2][1])
        dialog.findChild(QLineEdit, "le_username").setText(self.reddit_para[3][1])
        dialog.findChild(QLineEdit, "le_password").setText(self.reddit_para[4][1])
        dialog.findChild(QLineEdit, "le_apikey").setText(self.perspec_para[0][1])

        # Function Clickevent for dialog_box
        def clicked(button):
            if button == "Ok":
                self.bReturn = True
            else:
                self.bReturn = False

        # Connect the clickevent
        bBox = dialog.findChild(QDialogButtonBox, "dia_box")
        bBox.button(QDialogButtonBox.Ok).clicked.connect(partial(clicked, "Ok"))
        bBox.button(QDialogButtonBox.Cancel).clicked.connect(partial(clicked, "Cancel"))

        # Call after Dialogbox closed
        if dialog.exec_():
            if self.bReturn:
                values = [dialog.findChild(QLineEdit, "le_client_id").text(),
                          dialog.findChild(QLineEdit, "le_client_secret").text(),
                          dialog.findChild(QLineEdit, "le_user_agent").text(),
                          dialog.findChild(QLineEdit, "le_username").text(),
                          dialog.findChild(QLineEdit, "le_password").text(),
                          dialog.findChild(QLineEdit, "le_apikey").text()]

                self.config.setValues(values)
                self.reddit_para    = self.config.getRedditValues()
                self.perspec_para   = self.config.getPerspectiveValues()

    def displayComment(self):
        # Deklarations
        redFont     = "<font color=\"Red\">"
        greenFont   = "<font color=\"Green\">"
        endHTML     = "</font> "

        # Get Clicked Item
        if self.treeWidget.currentItem() is None:
            return

        item = self.comments[int(self.treeWidget.currentItem().text(1))]
        comment = item[4]

        # Clear Boxes
        self.comment_plaintextview.clear()
        self.pos_words.clear()
        self.neg_words.clear()

        # Set Text
        self.comment_plaintextview.insertPlainText(comment)

        # set scoring function
        if self.rbtn_afinn.isChecked():
            comment, words_score = self.afinn.score_words(comment)
        elif self.rbtn_senti.isChecked():
            comment, words_score = self.senti.score_words(comment)
        else:
            comment, words_score = self.perspective.score_words(comment)

        # Set Colored Words
        for word, score in zip(comment, words_score):
            if score >= 0:
                word = greenFont + word + endHTML
                self.pos_words.appendHtml(word)
            elif score < 0:
                word = redFont + word + endHTML
                self.neg_words.appendHtml(word)

    def __convert2decimal(self, number):
        return (int(number*100))/100


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    window = GUI()
    app.exec_()
