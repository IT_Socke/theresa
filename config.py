import configparser
import base64
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.fernet import Fernet


class Config:
    def __init__(self):
        self.__config = configparser.ConfigParser()
        self.__config.read('settings.ini')

    def __generateKey(self):
        password    = "Sola"
        password    = password.encode()
        salt        = b'Salzgott'
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=salt,
            iterations=100000,
            backend=default_backend()
        )
        key = base64.urlsafe_b64encode(kdf.derive(password))
        key = key.decode()
        return key

    def __encryptString(self, key, message):
        key         = key.encode()
        message     = message.encode()
        f           = Fernet(key)
        encrypted   = f.encrypt(message)
        encrypted   = encrypted.decode()
        return encrypted

    def __decryptString(self, key, encrypted):
        key         = key.encode()
        encrypted   = encrypted.encode()
        f           = Fernet(key)
        message     = f.decrypt(encrypted)
        message     = message.decode()
        return message

    def __checkKey(self):
        if 'security' in self.__config:
            key = self.__config['security']['key']
            if key == '':
                key = self.__generateKey()
                self.__config.set('security', 'key', key)

                if 'reddit' in self.__config:
                    for id in self.__config['reddit']:
                        value = self.__config['reddit'][id]
                        self.__config.set('reddit', id, self.__encryptString(key, value))

                with open('settings.ini', 'w') as configfile:
                    self.__config.write(configfile)

    def getRedditValues(self):
        # Deklaration
        values = []

        if 'security' in self.__config:
            key = self.__config['security']['key']
        else:
            return

        if 'reddit' in self.__config:
            for id in self.__config['reddit']:
                value = self.__config['reddit'][id]
                values.append([id, self.__decryptString(key, value)])

        return values

    def getPerspectiveValues(self):
        # Deklaration
        values = []

        if 'security' in self.__config:
            key = self.__config['security']['key']
        else:
            return

        if 'perspective' in self.__config:
            for id in self.__config['perspective']:
                value = self.__config['perspective'][id]
                values.append([id, self.__decryptString(key, value)])

        return values

    def setValues(self, values):
        if 'security' in self.__config:
            key = self.__config['security']['key']
        else:
            return

        if 'reddit' in self.__config:
            for id, value in zip(self.__config['reddit'], values[:5]):
                self.__config.set('reddit', id, self.__encryptString(key, value))

        if 'perspective' in self.__config:
            for id, value in zip(self.__config['perspective'], values[5:]):
                self.__config.set('perspective', id, self.__encryptString(key, value))

        with open('settings.ini', 'w') as configfile:
            self.__config.write(configfile)
