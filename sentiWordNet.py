import re
import nltk
import numpy as np
from nltk.corpus import stopwords


class SentiWordNet:
    def __init__(self, filePath):
        # Init - nltk donwloads
        nltk.download('stopwords')

        # Deklaration & Init
        self.__ex           = ""
        self.__sentiwordnet = self.__getSentiwordnet(filePath)

    def returnMessage(self):
        return self.__ex

    def __getSentiwordnet(self, filepath):
        # Deklaration
        data    = []

        # Open Datafile
        try:
            f = open(filepath)
        except Exception as ex:
            self.__ex = ex
            return

        # After line 26 end the comments
        for line in f.readlines()[26:]:
            cols    = line.split("\t")
            words   = self.__getWords(cols)
            for word in words:
                data.append([word, cols[2], cols[3]])

        return np.array(data)

    def __cleanComment(self, comment):
        comment = re.findall("[A-Za-z]+", comment.lower())

        stop_words = set(stopwords.words('english'))

        # Delete Stopwords
        comment = [w for w in comment if not w in stop_words]

        return comment

    def score(self, comment):
        idx     = []

        # Cleaning Comment
        comment = self.__cleanComment(comment)

        for word in comment:
            idx.extend(np.where(self.__sentiwordnet[:, 0] == word)[0])

        if len(idx) == 0:
            return 0

        senti_objs  = self.__sentiwordnet[idx]
        totalobject = self.__convert4decimal(self.__calcScore(senti_objs))

        # return total Score
        return totalobject

    def score_words(self, comment):
        # Deklaration
        words_score = []

        # Cleaning Comment
        comment = self.__cleanComment(comment)
        comment = list(dict.fromkeys(comment))

        for word in comment:
            idx = np.where(self.__sentiwordnet[:, 0] == word)[0]

            if len(idx) == 0:
                words_score.append(0)
            else:
                senti_objs = self.__sentiwordnet[idx]
                words_score.append(self.__calcScore(senti_objs))

        return comment, words_score

    def __getWords(self, cols):
        words_ids = cols[4].split(" ")
        words = [w.split("#")[0] for w in words_ids]
        return words

    def __calcScore(self, senti_objs):
        return np.mean(senti_objs[:, 1].astype(float) - senti_objs[:, 2].astype(float))

    def __convert4decimal(self, number):
        return (int(number*10000))/10000
