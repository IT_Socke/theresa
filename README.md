# Theresa

THE REddit Sentiment Analyzer

When you start the program for the first time, please open the settings and fill in the given fields. In order to fill in these fields you have to create several accounts. 
How to create these accounts, please refer to the points "reddit" and "perspective" below.

# Python package requirements
The program was developed and tested with Python version 3.7.6.
*  PyQT5
*  afinn
*  certifi
*  cryptography
*  more-itertools
*  numpy
*  praw
  

# settings
Only enter the settings described in the following two points. This means that you leave the [security] -> key item blank.

## reddit
Create a new account at "https://www.reddit.com/prefs/apps/". C
reate an application, give it any name and select script. The remaining fields 
can be left empty. Afterwards, all data can be taken from this application. The 
field "user_agend" in the settings has to be chosen freely and cannot be 
taken from the previously created account.

## perspective
Create an API key according to the following instructions 
"https://cloud.google.com/docs/authentication/api-keys#api_key_restrictions" 
and enter it under [perspective] -> apikey.
