import praw
import numpy as np
from praw.models import MoreComments


class RedditAnalyse:
    def __init__(self, url, client_id, client_secret, user_agent, username, password, limit=None):
        self.__redditHelper = praw.Reddit(client_id=client_id, client_secret=client_secret, user_agent=user_agent,
                                          username=username, password=password)
        self.__submission       = self.__redditHelper.submission(url=url)
        self.__submission.comments.replace_more(limit=limit)
        self.__comments_scores  = []

    def edit_article(self):
        comments_scores         = [[self.__submission.id, 0, "", "", self.__submission.title, self.__submission.ups]]
        comments_scores         = self.__recursion_comments(self.__submission.comments, comments_scores, 1,
                                                            self.__submission.id)
        self.__comments_scores  = comments_scores
        return comments_scores

    def __recursion_comments(self, comments, comment_score, level, parent):
        for top_comment in comments:
            if len(top_comment.replies) == 0:
                comment_score.append([top_comment.id, level, parent, "", top_comment.body, top_comment.ups])
            else:
                comment_score.append([top_comment.id, level, parent, "", top_comment.body, top_comment.ups])
                comment_score = self.__recursion_comments(top_comment.replies, comment_score, level+1, top_comment.id)

        return comment_score

    def getStatistics(self):
        scores  = np.array(np.take(self.__comments_scores, 3, axis=1)).astype(float)
        avg     = np.sum(scores) / len(scores)
        return avg, min(scores), max(scores)
